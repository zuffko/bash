#!/bin/bash
if ! [ -d $1 ] ; then
	echo 'Поиск невозможен, не существует директории'$1 
	exit 
elif  [[ -d $2 &&  -w $2 ]] ; then
	echo "Директория не верна или отсутствуют права для записи" $2
	exit 
fi
find $1 -iname '*.jpg' -o -iname '*.png' -o -iname '*.ico' -o -iname '*.gif' -o -iname '*.bmp' >$2
cat $2 | while read A 
do
 echo  ${A:21}
done
echo "Количество файлов=" 
cat $2 | wc -l